
from unittest import TestCase
from selenium import webdriver
from django.core.files.uploadedfile import SimpleUploadedFile

class FuncionalTest(TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def testTitle(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Busco Ayuda', self.browser.title)

    def testRegistro(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_register')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombres')
        nombre.send_keys('Carolina7')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Rozo7')

        experiencia = self.browser.find_element_by_id('id_experiencia')
        experiencia.send_keys('5')

        self.browser.find_element_by_xpath("//select[@id = 'id_servicio']/option[text()='Desarrollador Web']").click()

        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3112139593')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('ra.diana7@uniandes.edu.co')

        foto = self.browser.find_element_by_id('id_foto')
        foto.send_keys("C:\\Users\\DianaR\\Desktop\\img\\1.jpg")

        password = self.browser.find_element_by_id('id_password')
        password.send_keys('pass1234')

        passwordCon = self.browser.find_element_by_id('id_password_confirm')
        passwordCon.send_keys('pass1234')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()

        self.browser.implicitly_wait(5)
        h3 = self.browser.find_element_by_xpath("//h3[text()='Carolina7']")

        self.assertIn('Carolina7 Rozo7', h3.text)

    def test_detalle(self):
        self.browser.get('http://localhost:8000')

        self.browser.find_element_by_xpath("//select[@id = 'id_servicio']/option[text()='Desarrollador Web']").click()

        self.browser.find_element_by_id('id_filtrar').click()


        self.browser.implicitly_wait(5)
        h3 = self.browser.find_element_by_xpath("//h3[text()='Carolina Rozo']")

        self.assertIn('Carolina Rozo', h3.text)
